#define Current_sensor_pin_1 A0
#define coil_ratio 0.235 // V per Primairy amp
#define potmeter_pin A7
#define relais1_pin 3

float resistor_value = 470;

void setup() {
  Serial.begin(9600);
  if(Serial){
    Serial.println("Serial setup.");
    Serial.println("Setup values: ");
    Serial.print("measuring on pin :");
    Serial.println(Current_sensor_pin_1);
    Serial.print("resistor value :");
    Serial.println(resistor_value);
    Serial.print("coil ratio value :");
    Serial.println(coil_ratio);
    delay(500);
  }
  pinMode(Current_sensor_pin_1, INPUT);
  pinMode(potmeter_pin, INPUT);
  pinMode(relais1_pin, OUTPUT);
}

void loop() {
  float V = measure_voltage(Current_sensor_pin_1, potmeter_pin);
  Serial.print(" measured_voltage : ");
  Serial.print(V);
  Serial.print("  |   Calculated secondairy coil current :  ");
  float I_primairy = Secondairy_current_to_primairy_current(V, coil_ratio);
  Serial.print("  |   Primary current : ");
  Serial.println(I_primairy);
  if(I_primairy < 1.00){
    digitalWrite(relais1_pin, HIGH);
  }
  else {
    digitalWrite(relais1_pin, LOW);
  }
  delay(200);

}
/* inputs voltage and resistor value to output a current */
float voltage_to_current(float U, float R) {
  float I = U/R;
  return(I);
}
float Secondairy_current_to_primairy_current(float I_secondairy, float Ratio){
  float I_primairy = I_secondairy/Ratio;
  return(I_primairy);
}
float voltage_adjust(int pin){
  float V_adjust = (float) map(analogRead(pin), 0, 1023, -512, 512);
  V_adjust = (V_adjust*0.0049)*0.20;
  return(V_adjust);
}
float measure_voltage(int pin_coil, int pin_pot){
  float V_measured = (float) analogRead(pin_coil);
  V_measured = V_measured*0.0049+voltage_adjust(pin_pot);
  return (V_measured);
}


